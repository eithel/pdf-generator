# C1 - Custom PDF generation service

## Design a system to satisfy the following

1. **a user requests the generation of a new PDF file calling an HTTPs endpoint (with authentication, authorization is optional)**
2. **the user should be able to provide some data (e.g the customer name) to customize the PDF**
3. **the PDF is generated containing the user-provided data**
4. **the PDF is stored somewhere**
5. **the PDF generation is handled by a 3rdy part software**
6. **the PDF generation requires 3 hours**
7. **the user is notified about the generated PDF**
8. **the user can retrieve the generated PDF**
9. **what if the service fail somewhere?**

![Schema](custom-PDF-generation.svg "Custom PDF generation")

### Dettagli

L'architettura si suddivide in 3 componenti:

1. una componente responsabile alla ricezione del dato dell'utente, e con successivo passaggio di informazioni al servizio terzo di creazione PDF
2. una componente responsabile di notificare l'utente quando il servizio terzo ha concluso la generazione del PDF
3. una componente responsabile alla provisioning della risorsa creata (il file PDF)

Per ciascuna componente, andrò ad stimare i tempi di realizzazione, trade-off, sistema di deploy e aggiornamento e discuterò il sistema di monitoring.

### Dettaglio 1: ricezione dato in input dall'utente

Questo componente ha il compito principale di esporre un endpoint vero la parte pubblica, ricevere i dati in input dall'utente (_pallino 1-2 del grafico_), verificare i dati tramite una lambda function e passare i dati al servizio terzo (_pallino 5 del grafico_).

Si suppone che in questo caso, la richiesta HTTPs avvenga da parte di un utente già autenticato nel sistema, ed essa avverrà tramite una richiesta POST dove il _body_ conterrà il JSON con i dati del PDF. La richiesta inoltre conterrà anche il Token JWT che identifica l'utente.

In questa soluzione non viene presa in considerazione la parte di front-end, la quale è indipendente dal sistema.  

La lambda una volta ricevuto il dato, per prima cosa verifica l'identità dell'utente chiamando un servizio apposito per la verifica dell'utente (_pallino 3 del grafico_). In questa soluzione si presuppone che un IdP esterno, dato un JWT, possa verificare l'effettiva correttezza del JWT.  
Tale controllo, interno alla lambda, potrebbe essere demandato a qualche altro servizio (Lambda@Edge nella richiesta POST) così da togliere complessità alla lambda stessa.

La lambda dopo aver identificato l'utente, valida il contenuto del _body_ della richiesta: se esso non è corretto, la lambda restituisce un errore 400 al client.

Una volta verificato il _body_, la lambda salverà la richiesta nel database collegandola all'utente (_pallino 4 del grafico_), mettendo la richiesta in uno stato di _accepted_. Se il salvataggio a database non va a buon fine, la lambda ritorna un errore 503 al client: senza questo primo salvataggio non si prosegue alla creazione del PDF.  
La lavorazione avrà un id univoco che permette di individuare la lavorazione sia all'interno del nostro sistema, sia all'interno del servizio terzo di generazione PDF e del PDF stesso.

Se il salvataggio a database va a buon fine, la lambda cerca di inviare i dati al servizio terzo di generazione PDF (_pallino 5 del grafico_).  
Tale servizio, si presuppone che sia veloce nel accettare la richiesta (la vera generazione avviene in modalità asincrona), quindi la lambda restituirà un messaggio con status code 201.

Se il servizio terzo per qualche ragione non risponde, comunque all'utente verrà restituito un 201. 
Sarà compito di un cron job separato (_pallino 16 del grafico_) recuperare eventuali lavori "pending" e passarli al servizio terzo di generazione PDF. Questo cron job potrebbe richiamare la stessa lambda: in questo caso si dovrebbe pensare ad una lambda che accetti un possibile ID di lavorazione con il metodo PUT. Inoltre compito del cronjob prepare un JWT valido per la richiesta HTTPs. La lambda, dato il metodo PUT e la presenza dell'ID non crea una nuova lavorazione, ma aggiorna quella già presente. Questa "rotta HTTP" poiché avrà una JWT "privileggiato", deve essere in una VPC dedicata così da limitarne eventuale abusi.

In caso di avvenuta ricezione da parte del servizio terzo, si andrà ad aggiornare il database, aggiornando lo stato della lavorazione mettendolo _in progress_.  
**Nota:** potrebbe fallire questa seconda scrittura nel database. Qui possiamo adottare diverse strategie per mitigare questo caso: una prima strategia che dipende dal servizio terzo di generazione PDF è quella di usare il cron job che richiama la lambda che ri-manda la lavorazione al servizio terzo e aggiorna il database. In questo caso il servizio terzo dovrebbe avere la capacità di "riconoscere" una precedente lavorazione e non attivarne un'altra.  
Un'altra strategia è quella di salvare in una coda solo l'attività di aggiornamento del database.  
Un'altra strategia ancora è quella di usare degli CloudWatch Events che rispondono a determinati eventi e che potrebbero richiamare una funzione di aggiornamento database.

In ogni caso, cerchiamo di dare all'utente un'esperienza seamless all'utente, dove cerchiamo sempre di prendere in carico il lavoro, anche in caso di possibili disservizi.

Tutto questa parte dell'architettura, userà CloudWatch (_pallino 15 del grafico_) per loggare eventi normali ed eccezionali, così da poter creare eventuali metriche e allarmi. 

Per completare l'attività si prevede un determinato tempo di DevOps per il setup dei diversi servizi (2-3 giorni) e poi per lo sviluppo vero e proprio 7-9 giorni. A termine metterei 2 giornate di QA per gli eventuali test di integrazione.


### Dettaglio 2: invio notifica all'utente

Questo componente dell'architettura ha il compito di avvisare l'utente che il suo PDF è pronto e può essere recuperato.

In questo caso si presuppone che il servizio terzo di generazione PDF collochi il PDF generato in un bucket condiviso (_pallino 6 del grafico_).  
Una volta che il PDF è stato collocato nel bucket, viene attivata una lambda (_pallino 7 del grafico_), che ha il compito di aggiornare il database (_pallino 8 del grafico_) aggiornando lo stato in _ready_ e preparando il link di download (salvato nel database, se non immediatamente ricondubile all'id della lavorazione) da passare al sistema di notifica (_pallino 9 del grafico_), il quale avrà il compito di notificare l'utente.

Qui demandiamo la responsabilità di scegliere la modalità di notifica (email, push notification, etc) e di prepare la l'effettiva notifica al sistema di notifica (_pallino 9 del grafico_).

In caso di errore nell'aggiornamento del database, non verrà inviata nessuna notifica, e sarà presente un cron job (_pallino 16 del grafico_) che andrà a recuperare eventuali lavorazioni _in progress_ (con un creation time superiore alle 3 ore) e che andrà a controllare se il PDF è presente nel bucket e di conseguenza rilanciare la funzione lambda.

Particolare attenzione per quanto riguarda i permessi per l'accesso al bucket: i permessi dati al servizio terzo devono essere limitati alla sola crezione di oggetti e non alla consultazione o al download.

Tutto questa parte dell'architettura, userà CloudWatch (_pallino 15 del grafico_) per loggare eventi normali ed eccezionali, così da poter creare eventuali metriche e allarmi. 

Per completare l'attività si prevede un determinato tempo di DevOps per il setup dei diversi servizi (1-2 giorni) e poi per lo sviluppo vero e proprio 5-7 giorni. A termine metterei 1 giornata di QA per gli eventuali test di integrazione. Non è considerata in questa stima eventuali tempi per setup / realizzazione del sistema di notifica.

### Dettaglio 3: Utente accede per recupeare il pdf

Questo componente dell'architettura ha il compito principale di servire il PDF all'utente autenticato.

Il sistema di notifica invia un link per il donwload del PDF (diretto, o comunque dentro un'area personale dell'utente). Una volta che l'utente tenta di usare il link (_pallini 10 e 11 del grafico_), ci sarà una una lambda che, come per il "Dettaglio 1" verifica che l'utente sia correttamente loggato usando l'IdP (_pallino 12 del grafico_) e poi legge il database per individuare il file corretto (_pallino 13 del grafico_). **Nota:** quest'ultima parte, se ben pensata (uso di id univoco nel link di download) potrebbe essere evitata, riducendo la complessità del sistema. Se comunque si deve garantire una tranciabilità in termini di accessi / operazioni svolte dall'utente, si possono usare altri tool (tipo CloudWatch).

Trovato il filename da servire, la lambda serve il file recuperandolo dal bucket (_pallino 14 del grafico_).

Anche questa parte dell'architettura, userà CloudWatch (_pallino 15 del grafico_) per loggare eventi normali ed eccezionali, così da poter creare eventuali metriche e allarmi. 

Per completare l'attività si prevede un determinato tempo di DevOps per il setup dei diversi servizi (1 giorni) e poi per lo sviluppo vero e proprio 5 giorni. A termine metterei 1 giornata di QA per gli eventuali test di integrazione.

## Considerazioni globali architettura

Il team di sviluppo, supponendo di avere 3 persone, una volta concordato lo schema del database, e modalità di tracciamento dei log, può lavorare in modo indipendente e in parallelo sui 3 componenti.

In questa analisi non è stato considerato eventuali metriche di tracciamo del comportamento dell'utente utili quando si ha un utente loggato che interagisce con il prodotto.

Il team di DevOps assieme al team di sviluppo, dovrebbe concordare sui possibili errori dell'applicazione, in modo tale da poter creare delle metriche utili così da creare un sistema di monitoring in modo tale da intercettare eventuali deterioramenti del sistema.

Poiché l'architettura è stata spezzata in più componenti, ogni componente può essere visto come un pezzo indipendente con proprie politiche di deploy e di aggiornamento. Per il deploy si consigliano sistemi di CI/CD per la delivery dell'applicazione nei diversi ambienti.

La soluzione, bastata su lambda, adatta il costo all'effettivo uso da parte degli utenti.

Nel "dettaglio 3", potremmo inoltre prevedere uno strato di cache in modo tale da servire una versione cached del PDF.  
Questo componente, potrebbe essere di molto semplificato se ad esempio non è necessaria la lambda per controllare l'autenticazione dell'utente e per recuperare il link dal database, riducendo di fatto i servizi coinvolti e semplificando il tutto all'accesso diretto al file nel bucket (o usando servizi tipo Amazon S3 Object Lambda).  
